# Hello there folks!
Here comes the *latest submission of all times*. Well, almost. 
Here is the **List of my expectations:**
* Learn useful techniques to become more efficient while conducting a reaserch
* Learn R
* Get a hand of Python
* Continue to silently worship the inventor of the font Dr Koentges is using in his presentations
* Enjoy _Doctor Who_ references
  * However, I am at great risk in this class: I am at the end of the season 3 at the moment, so it is waaaay too easy for you guys to spoil everything for me. I mean, I can't anticipate what slides will come up and I sort of *have to* look at them...

#### This is a header for a link:
  Anything Jay Foreman does on [Youtube] (https://www.youtube.com/) is pretty much fantastic. To give you a little bit of an insight, [here is a recent one I found quite glorious] (https://youtu.be/r-aIzkvPwFo).  
You're welcome.
